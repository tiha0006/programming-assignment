import myIPconverter
import myTimeZone
import myDistanceconv



########## Main ###########################################

def main():
    ans=True
    while ans:
        print("""
        1.Time zone converter
        2.IPv4 to IPv6 Converter
        3.Km/Mile - Miles/Km converter
        4.Exit/Quit
        """)
        ans=input("What would you like to do? ")

        if ans=="1":
            myTimeZone.time_zone()
        elif ans=="2":
            myIPconverter.IPconverter()
        elif ans=="3":
            myDistanceconv.fibonacci(int(input("Number of iteration: ")))
        elif ans=="4":
            print("\n Goodbye")
            ans = None
        else:
            print("\n Not Valid Choice Try again")

main()
