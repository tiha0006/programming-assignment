#!/usr/bin/env python3
# Time zone converter
import datetime
import time
import sys
import msvcrt
import os

diff = datetime.timedelta(hours=-1)
timezones = [
    ["USA West",                            "PST",      datetime.timedelta(hours=-8)], #Pacific Time zone (US west)],
    ["USA Central",                         "CST",      datetime.timedelta(hours=-6)], #Central Time zone (US central)],
    ["USA East",                            "EST",      datetime.timedelta(hours=-5)], #Eastern Time zone (US east)],
    ["Central European time",               "CET",      datetime.timedelta(hours=1)], #Central European time
    ["India Standard time",                 "IST",      datetime.timedelta(hours=5.5)], #India standard time
    ["China Standard time",                 "CST",      datetime.timedelta(hours=8)], #China standard time
    ["Australia West Time zone",            "AWST",     datetime.timedelta(hours=8)], #Australia west Time zone
    ["Australia Central Standard time",     "JST",      datetime.timedelta(hours=9)], #Japan Time zone
    ["Australia East Time zone",            "ACST",     datetime.timedelta(hours=9.5)],  #Australia central standard
    ["Japan Time zone",                     "AET",      datetime.timedelta(hours=11)] #Australia east time zone
]

current_time = 0
utc = 0
def update_time():
    global current_time
    global utc
    current_time = datetime.datetime.now() # gets the local time (only 1 time)
    utc = current_time + diff # Coordinated Universal Time

def show_time(tz):
    print ("Press <ESC> for exit ")
    while True:
        update_time() #updates time for "real time" printing
        t = utc + tz[2]
        sys.stdout.write("\r" + (utc.strftime(" %H:%M:%S") + " UTC  " + "--> " + (t.strftime("%H:%M:%S")) + " " + tz[1]))
        sys.stdout.flush() # this 2 lines prints, clears the print and repints until we break out from loop
        time.sleep(1)
        if msvcrt.kbhit():                 #keyboard command
            if ord(msvcrt.getch()) == 27: #chr 27 stands for <ESC>
                break                       #braks the loop with <Esc>

def time_zone():
    menustring = ""
    for tz in range(len(timezones)):
        menustring += str(tz+1) + "." + timezones[tz][0] + "\n"
    menustring += "q.Exit/Quit"

    while True:              # converter menu
        os.system('cls')    #clear the screen

        print (menustring)
        ans=input("Which Timezone are you interested in? : ")

        if(ans == "q"):
            print("\n Goodbye")
            ans = None
            break

        #### Error Checking ####
        try:
            ans = int(ans)
        except ValueError as e:
            print("Enter a valid number")
            continue

        if(ans < 1 or ans > len(timezones)):
            print("Enter a valid number")
            continue

        show_time(timezones[ans-1])
