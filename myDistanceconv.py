def fibonacci(n):
    fibolist=[]
    a,b = 0,1
    for i in range (n-1):
        a,b = b, a+b
        fibolist.append(a)

    with open("fibo.txt", "w") as f:
        for i in (fibolist):
            f.write(str(i+1) + "\n")

    gratio = [fibolist[i]/float(fibolist[i-1]) for i in range(0,len(fibolist))]

    with open("gratio.txt", "w") as f:
        for i in (gratio):
            f.write(str(i) + "\n")

    ans=True
    while ans:
        print("""
        1.Miles to Km
        2.Km to Miles
        3.Exit/Quit
        """)
        ans=input("Choose conversion: ")

        if ans=="1":
            mile_input = int(input("Miles = "))
            km = mile_input*float(gratio[-1])
            print (str(mile_input) + " Miles = " + str(km) + " Km \n")

        elif ans=="2":
            km_input = int(input("Km = "))
            miles = km_input/float(gratio[-1])
            print (str(km_input) + " Km = " + str(miles) + " Miles \n")

        elif ans=="3":
            print("\n Goodbye")
            ans = None
        else:
            print("\n Not Valid Choice Try again")
