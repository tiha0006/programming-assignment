import ipaddress

def IPconverter():
    ans = True
    while ans:
        print("""
        s.Start
        q.Exit/Quit
        """)
        ans=input("Select an option:  ")

        if ans=="s":
            ipv4address=input("Type an IPv4 address: ")
            ip=(ipaddress.IPv6Address('2002::' + ipv4address).compressed)
            print ("IPv6 form: " +ip + "\n")
        elif ans=="q":
            print("\n Goodbye")
            ans = None
        else:
            print("\n Not Valid Choice Try again")
